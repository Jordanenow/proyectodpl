let Libro = function (id, nombre, genero) {

    this.id = id;

    this.nombre = nombre;

    this.genero = genero;

    

}

Libro.allLibros = [];

Libro.add = function (libro) {

    this.allLibros.push(libro);

}

let a = new Libro(1,"El Señor de los Anillos","Fantasia");

let b = new Libro(2, "Harry Potter","Fantasia");

// Borramos libros
Libro.removeById = function(aLibroId) {

    for (let i=0; i< Libro.allLibros.length; i++) {

        if (Libro.allLibros[i].id == aLibroId) {

            Libro.allLibros.splice(i,1);

            break;

        }

    }

}

Libro.findById = function(aLibroId) {

    let aLibro = Libro.allLibros.find(x => x.id == aLibroId);

    if (aLibro)

        return aLibro;

    else

        throw new Error(`No existe una bicicleta con el id ${aLibroId}`);

}

let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let libroSchema = new Schema({

    libroID: Number,
   
    nombre: String,
   
    genero: String,
   
   
   });

   //Metodos del modelo
   libroSchema.statics.allLibros = function (cb) {
    return this.find({}, cb);

};

libroSchema.statics.add = function(aLibro, cb) {

    return this.create(aLibro, cb);

};

module.exports = mongoose.model ("Libro", libroSchema);
